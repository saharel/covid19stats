from datetime import datetime
from collections import OrderedDict
import json
import urllib.request

import boto3
import pandas as pd

DATE_FORMAT = "%Y-%m-%d"
COVID19_API_URL = "https://covid-api.mmediagroup.fr/v1"
BUCKET = "covid19-statistics-cyber"


def upload_statistics_to_s3(statistics):
    s3 = boto3.resource('s3')
    for key, value in statistics.items():
        s3.Bucket(BUCKET).put_object(Key=key, Body=json.dumps(value))


def sort_dict_by_date(the_dict):
    # Python dicts do not hold their ordering so we need to make it an
    # ordered dict, after sorting.
    return OrderedDict(reversed(sorted(
        the_dict.items(),
        key=lambda x: datetime.strptime(x[0], DATE_FORMAT)
    )))


def lambda_handler(event, context):

    def get_total_recovered_last_week(dates):
        ordered_dates = sort_dict_by_date(dates)
        ordered_dates_values_as_list = list(ordered_dates.values())

        return ordered_dates_values_as_list[0] - ordered_dates_values_as_list[7]

    def get_avg_recovered_per_100_square_km(sq_km_area, dates):
        ordered_dates = sort_dict_by_date(dates)
        ordered_dates_values_as_list = list(ordered_dates.values())

        return ordered_dates_values_as_list[0]/sq_km_area

    def get_recovered_in_last_10_days_per_capita(population, dates):
        ordered_dates = sort_dict_by_date(dates)
        ordered_dates_values_as_list = list(ordered_dates.values())

        count = ordered_dates_values_as_list[0] - ordered_dates_values_as_list[10]

        return count / population

    # fetch recovered data history
    with urllib.request.urlopen(f"{COVID19_API_URL}/history?status=recovered") as url:
        data = json.loads(url.read().decode())

    # parse to dataframe
    df = pd.DataFrame.from_dict(data, orient='index')[['All']]
    df = df["All"].apply(pd.Series)

    # remove the global row - as it is not a country, and we don't need it
    df = df.drop('Global')

    # add a column for recovered in last 10 days per capita
    df["recovered_in_last_10_days_per_capita"] = \
        df.apply(lambda row: get_recovered_in_last_10_days_per_capita(row.population, row.dates), axis=1)

    # add a column for total amount of recovered people
    df["total_recovered_last_week"] = df.apply(lambda row: get_total_recovered_last_week(row.dates), axis=1)

    # add a column for average recovered cases per 100 square KM
    df["avg_recovered_per_100_square_km"] =\
        df.apply(lambda row: get_avg_recovered_per_100_square_km(row.sq_km_area, row.dates), axis=1)

    # calculate needed statistics
    df_top_recovered_in_last_10_days_per_capita = df.sort_values("recovered_in_last_10_days_per_capita", ascending=False).head(5)
    df_top_total_recovered_last_week = df.sort_values("total_recovered_last_week", ascending=False).head(10)
    avg_recovered_per_100_square_km = df["avg_recovered_per_100_square_km"].mean()

    # upload gathered statistics to s3 bucket
    statistics = {
        "top_recovered_in_last_10_days_per_capita": list(df_top_recovered_in_last_10_days_per_capita["country"].values),
        "top_total_recovered_last_week": list(df_top_total_recovered_last_week["country"].values),
        "avg_recovered_per_100_square_km": avg_recovered_per_100_square_km,
    }
    upload_statistics_to_s3(statistics)
